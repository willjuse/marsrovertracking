# README #

### What is this repository for? ###

* Arrive coding challenge

### How do I get set up? ###

* .NET Core 2.0
* VS 2017
* Run the project with VS and get the sessionId
* Use Postman (or similar) to hit end points

### Endpoints

* GET api/movement/{roverId} if it doesn't exist it will be created at 0, 0 facing North
* POST api/movement/{roverId} {"movement": "[userString]"}
