﻿using Microsoft.EntityFrameworkCore;

namespace MarsRoverTracking.Models
{
    public sealed class LocationAndDirectionContext : DbContext
    {
        public LocationAndDirectionContext(DbContextOptions<LocationAndDirectionContext> options)
            : base(options)
        {
        }

        public DbSet<LocationAndDirectionModel> LocationAndDirections { get; set; }
    }
}
