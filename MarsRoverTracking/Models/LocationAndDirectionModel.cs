﻿namespace MarsRoverTracking.Models
{
    public class LocationAndDirectionModel
    {
        // note database generates the id when an item is created
        public long Id { get; set; }
        public string RoverId { get; set; }
        public DirectionEnumeration Direction { get; set; }
        public (int x, int y) CurrentLocation { get; set; }
    }
}
