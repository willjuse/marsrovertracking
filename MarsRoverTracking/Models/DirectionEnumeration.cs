﻿namespace MarsRoverTracking.Models
{
    public enum DirectionEnumeration
    {
        // Up
        North = 0,

        // Right
        East = 1,

        // Down
        South = 2,

        // Left
        West = 3
    }
}
