﻿using MarsRoverTracking.Models;
using System.Collections.Generic;
using System.Linq;
using System;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace MarsRoverTracking.Controllers
{
    // this sets up the address to be api/movement/
    [Route("api/[controller]")]
    public class MovementController : Controller
    {
        private readonly LocationAndDirectionContext context;

        public MovementController(LocationAndDirectionContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            this.context = context;
        }

        public string Get()
        {
            return "";
        }

        [HttpGet("{roverId}", Name="GetLocation")]
        public IActionResult GetLocation(string roverId)
        {
            if (roverId.Length > 10)
            {
                return BadRequest();
            }

            var location = this.context.LocationAndDirections.FirstOrDefault(x => x.RoverId == roverId);
            if (location != null)
            {
                return Json(new PostResult($"Found {roverId} facing {location.Direction}", $"({location.CurrentLocation.x}, {location.CurrentLocation.y})"));
            }

            location = new LocationAndDirectionModel
            {
                CurrentLocation = (0, 0),
                Direction = DirectionEnumeration.North,
                RoverId = roverId
            };

            this.context.LocationAndDirections.Add(location);
            this.context.SaveChanges();
            return CreatedAtRoute("GetLocation", location);
        }

        [HttpPost("{roverId}", Name="Update")]
        public IActionResult Update(string roverId, [FromBody] MovementInstructionModel movementInstruction)
        {
            string movementInstructions = movementInstruction.MovementInstruction;
            LocationAndDirectionModel location = this.context.LocationAndDirections.FirstOrDefault(x => x.RoverId == roverId);
            if (movementInstructions.Length > 100)
            {
                // TODO we could add something for if movement contains something other than R, L, or M
                return BadRequest();
            }

            if (location == null)
            {
                return NotFound();
            }

            // save the start to write a clean message
            LocationAndDirectionModel startLocation = new LocationAndDirectionModel
            {
                RoverId = location.RoverId,
                CurrentLocation = (location.CurrentLocation.x, location.CurrentLocation.y),
                Direction = location.Direction,
                Id = location.Id
            };

            var currentDirection = location.Direction;

            foreach (char c in movementInstructions)
            {
                switch (c)
                {
                    case 'L':
                        switch (currentDirection)
                        {
                            case DirectionEnumeration.North:
                                location.Direction = DirectionEnumeration.West;
                                break;
                            case DirectionEnumeration.East:
                                location.Direction = DirectionEnumeration.North;
                                break;
                            case DirectionEnumeration.South:
                                location.Direction = DirectionEnumeration.East;
                                break;
                            case DirectionEnumeration.West:
                                location.Direction = DirectionEnumeration.South;
                                break;
                        }

                        currentDirection = location.Direction;
                        break;
                    case 'R':
                        switch (currentDirection)
                        {
                            case DirectionEnumeration.North:
                                location.Direction = DirectionEnumeration.East;
                                break;
                            case DirectionEnumeration.East:
                                location.Direction = DirectionEnumeration.South;
                                break;
                            case DirectionEnumeration.South:
                                location.Direction = DirectionEnumeration.West;
                                break;
                            case DirectionEnumeration.West:
                                location.Direction = DirectionEnumeration.North;
                                break;
                        }

                        currentDirection = location.Direction;
                        break;
                    case 'M':
                        switch (currentDirection)
                        {
                            case DirectionEnumeration.North:
                                location.CurrentLocation = (location.CurrentLocation.x, location.CurrentLocation.y + 1);
                                break;
                            case DirectionEnumeration.East:
                                location.CurrentLocation = (location.CurrentLocation.x + 1, location.CurrentLocation.y);
                                break;
                            case DirectionEnumeration.South:
                                location.CurrentLocation = (location.CurrentLocation.x, location.CurrentLocation.y - 1);
                                break;
                            case DirectionEnumeration.West:
                                location.CurrentLocation = (location.CurrentLocation.x - 1, location.CurrentLocation.y);
                                break;
                        }
                        break;
                    default:
                        return BadRequest();
                }
            }

            string message = BuildMessage(startLocation, location);
            context.SaveChanges();
            return Json(new PostResult(message, $"({location.CurrentLocation.x}, {location.CurrentLocation.y})"));
        }

        private static string BuildMessage(LocationAndDirectionModel start, LocationAndDirectionModel end)
        {
            string message = "Rover moved ";
            int diffX = end.CurrentLocation.x - start.CurrentLocation.x;
            int diffY = end.CurrentLocation.y - start.CurrentLocation.y;

            if (diffX >= 0)
            {
                message += $"{diffX} spaces right ";
            }
            else
            {
                message += $"{Math.Abs(diffX)} spaces left ";
            }

            message += "and ";

            if (diffY >= 0)
            {
                message += $"{diffY} spaces up";
            }
            else
            {
                message += $"{Math.Abs(diffY)} spaces down";
            }

            return message;
        }

        private sealed class PostResult
        {
            public string Message { get; set; }

            public string CurrentPosition { get; set; }

            public PostResult(string message, string currentPosition)
            {
                this.Message = message;
                this.CurrentPosition = currentPosition;
            }
        }
    }
}
